describe("App E2E", () => {
  it("should have a input", () => {
    cy.visit("/");
    cy.get("input").should("have.value", "");
  });

  const findReact = () => {
    cy.get("input").type("React").should("have.value", "React");
    cy.url().should("include", "search=React");
  };
  it("current pagination state", () => {
    cy.visit("/");
    findReact();
    cy.contains("Back").should("be.disabled");
    cy.contains("Next").should("not.be.disabled");
    cy.get(".repository-card");
  });

  it("open repository", () => {
    cy.visit("/");
    findReact();
    cy.get(".repository-card__name").first().click();
    cy.get(".single-repository__owner-name");
  });

  it("Should not find repositories", () => {
    cy.visit("/");
    cy.get("input").type("23@##@#@rsdrer2134$@$!%!@%#%$%#^%@#^");
    cy.get(".repository-card__name").should("have.lengthOf", 0);
    cy.contains("No matching items");
  });
});
