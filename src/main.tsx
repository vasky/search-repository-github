import * as ReactDOM from "react-dom/client";
import { createBrowserRouter, RouterProvider } from "react-router-dom";

// styles
import "./styles/fonts.css";
import "./styles/index.css";

import { ROUTES } from "./enums/routes";
import Home from "./pages/home";
import SingleRepository from "./pages/single-repository/single-repository";
import { ApolloProvider } from "@apollo/client";
import client from "./apollo/client";
import { QueryParamProvider } from "use-query-params";
import { ReactRouter6Adapter } from "use-query-params/adapters/react-router-6";

const router = createBrowserRouter([
  {
    path: ROUTES.HOME,
    element: (
      <QueryParamProvider adapter={ReactRouter6Adapter}>
        <Home />
      </QueryParamProvider>
    ),
  },
  {
    path: ROUTES.REPOSITORY,
    element: <SingleRepository />,
  },
]);

ReactDOM.createRoot(document.getElementById("root") as HTMLElement).render(
  <ApolloProvider client={client}>
    <RouterProvider router={router} />
  </ApolloProvider>
);
