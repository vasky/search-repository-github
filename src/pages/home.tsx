import React, { useEffect } from "react";
import Layout from "../components/layout/layout";
import SearchBar from "../components/search-bar/search-bar";
import Repositories from "../components/repositories/repositories";
import Pagination from "../components/pagination/pagination";
import { useQuery } from "@apollo/client";
import { SEARCH_REPOSITORIES } from "../apollo/repositories";
import { StringParam, useQueryParams } from "use-query-params";
import { Repository } from "../models/repository";
import Loader from "../components/loader/loader";
import { PageInfo } from "../models/page-info";
import { PaginationDirection } from "../enums/pagination-direction";
import { useLocation } from "react-router-dom";
import { useUserStore } from "../store/user-store";

const Home = () => {
  const [query, setQuery] = useQueryParams({
    search: StringParam,
    after: StringParam,
    before: StringParam,
  });
  const location = useLocation();
  const userStore = useUserStore();

  const { search, after, before } = query;

  useEffect(() => {
    if (userStore.reps.length === 0) {
      userStore.getUserReps();
    }

    if (!location.state) {
      return;
    }

    const { before, after, search } = location.state;
    setQuery({
      before,
      after,
      search,
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const { data, loading } = useQuery<{
    search: { nodes: Repository[]; pageInfo: PageInfo };
  }>(SEARCH_REPOSITORIES, {
    skip: !search,
    variables: { query: search, after, before },
  });

  const handleChangePage = (direction: PaginationDirection) => {
    if (direction === PaginationDirection.PREV) {
      setQuery({
        before: data.search.pageInfo.startCursor,
        after: null,
      });
    } else {
      setQuery({
        before: null,
        after: data.search.pageInfo.endCursor,
      });
    }
  };

  const repositoriesToRender = () => {
    if (!search?.length && !userStore.reps.length) {
      return <div>Write something</div>;
    }

    const itemsReps = data?.search?.nodes || userStore.reps;

    if (itemsReps?.length) {
      return (
        <>
          <Repositories items={itemsReps || []} />
          <Pagination
            hasPrevPage={data?.search?.pageInfo.hasPreviousPage}
            hasNextPage={data?.search?.pageInfo.hasNextPage}
            onChangePage={handleChangePage}
          />
        </>
      );
    }

    return <div>No matching items</div>;
  };

  return (
    <Layout>
      <SearchBar
        value={search}
        onChangeSearchValue={(nextSearchValue) =>
          setQuery({ after: null, before: null, search: nextSearchValue })
        }
      />
      {loading || userStore.loadingReps ? <Loader /> : repositoriesToRender()}
    </Layout>
  );
};

export default Home;
