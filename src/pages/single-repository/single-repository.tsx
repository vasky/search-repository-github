import React from "react";
import Layout from "../../components/layout/layout";
import BackArrowButton from "../../components/back-arrow-button/back-arrow-button";
import { ROUTES } from "../../enums/routes";
import SingleRepositoryView from "./single-repository-view";
import { useQuery } from "@apollo/client";
import { Repository } from "../../models/repository";
import { useParams } from "react-router-dom";
import { SEARCH_SINGLE_REPOSITORY } from "../../apollo/single-repository";
import Loader from "../../components/loader/loader";

const SingleRepository = () => {
  const { repository } = useParams();

  const { data, loading } = useQuery<{
    node: Repository;
  }>(SEARCH_SINGLE_REPOSITORY, {
    skip: !repository,
    variables: { id: repository },
  });

  if (loading) {
    return (
      <div className="single-repository">
        <Loader />
      </div>
    );
  }

  return (
    <Layout>
      <BackArrowButton to={ROUTES.HOME}>Back to home</BackArrowButton>
      {data && <SingleRepositoryView rep={data.node} />}
    </Layout>
  );
};

export default SingleRepository;
