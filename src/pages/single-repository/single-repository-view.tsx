import React from "react";
import { Repository } from "../../models/repository";
import IconStar from "../../components/icons/icon-star";
import { NavLink } from "react-router-dom";
import "./single-repository.css";
import formatDistanceToNow from "date-fns/formatDistanceToNow";

interface SingleRepositoryViewProps {
  rep: Repository;
}

const SingleRepositoryView = ({
  rep: { owner, name, description, languages, stargazerCount, updatedAt, url },
}: SingleRepositoryViewProps) => {
  return (
    <div className="single-repository">
      <div className="single-repository__top">
        <div className="single-repository__avatar-wrapper">
          <img className="single-repository__avatar" src={owner.avatarUrl} />
        </div>
        <NavLink to={owner.url} className="single-repository__owner-name">
          {owner.login}
        </NavLink>
      </div>
      <div className="single-repository__content">
        <NavLink to={url} className="single-repository__name">
          {name}
        </NavLink>
        <div className="single-repository__description">{description}</div>
        <div className="single-repository__languages">
          {languages?.nodes.map((language) => (
            <span key={language.id} className="single-repository__language">
              <span
                className="single-repository__language-color"
                style={{ background: language.color }}
              />
              <span className="single-repository__language-name">
                {language.name} {language.size}
              </span>
            </span>
          ))}
        </div>
        <div className="single-repository__info">
          <div className="single-repository__stars">
            <IconStar />
            <span className="single-repository__stars-number">
              {stargazerCount}
            </span>
          </div>
          <div className="single-repository__separate">·</div>
          <div className="single-repository__updated">
            Updated {formatDistanceToNow(new Date(updatedAt))}
          </div>
        </div>
      </div>
    </div>
  );
};

export default SingleRepositoryView;
