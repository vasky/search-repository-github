import { create } from "zustand";
import { Repository } from "../models/repository";
import { makeRequest } from "../libs/make-request";
import { USER_REPOSITORIES_QUERY } from "../apollo/user-repositories";

interface UseUserState {
  reps: Repository[];
  loadingReps: boolean;
  getUserReps: () => void;
}

export const useUserStore = create<UseUserState>((set) => ({
  reps: [],
  loadingReps: false,
  getUserReps: async () => {
    set({ loadingReps: true });
    const userInfoReponse: {
      data: { viewer: { repositories: { edges: { node: Repository }[] } } };
    } = await makeRequest(USER_REPOSITORIES_QUERY).finally(() => {
      set({ loadingReps: false });
    });

    const nextUserReps = userInfoReponse.data.viewer.repositories.edges.map(
      (repository) => ({ ...repository.node })
    );

    set({ reps: nextUserReps });
  },
}));
