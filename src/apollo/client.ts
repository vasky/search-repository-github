import { ApolloClient, InMemoryCache } from "@apollo/client";

const client = new ApolloClient({
  uri: import.meta.env.VITE_HOST_GRAPHQL,
  cache: new InMemoryCache(),
  headers: {
    Authorization: import.meta.env.VITE_ACCESS_TOKEN,
  },
});

export default client;
