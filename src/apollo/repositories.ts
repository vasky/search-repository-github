import { gql } from "@apollo/client";

export const SEARCH_REPOSITORIES = gql`
  query SearchRepository($query: String!, $after: String, $before: String) {
    search(
      query: $query
      first: 10
      type: REPOSITORY
      after: $after
      before: $before
    ) {
      nodes {
        ... on Repository {
          name
          id
          stargazerCount
          updatedAt
          shortDescriptionHTML
          description
          owner {
            id
            avatarUrl
            login
          }
        }
      }
      pageInfo {
        endCursor
        startCursor
        hasNextPage
        hasPreviousPage
      }
    }
  }
`;
