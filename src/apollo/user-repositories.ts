export const USER_REPOSITORIES_QUERY = `
  query {
    viewer {
      repositories(first: 50) {
        edges {
          node {
            name
            id
            stargazerCount
            updatedAt
            shortDescriptionHTML
            description
            owner {
              id
              avatarUrl
              login
            }
          }
        }
      }
    }
  }
`;
