import { gql } from "@apollo/client";

export const SEARCH_SINGLE_REPOSITORY = gql`
  query SearchRepository($id: ID!) {
    node(id: $id) {
      ... on Repository {
        name
        id
        stargazerCount
        updatedAt
        shortDescriptionHTML
        description
        url
        languages(first: 4) {
          nodes {
            color
            id
            name
          }
        }
        owner {
          id
          avatarUrl
          login
          url
        }
      }
    }
  }
`;
