export const makeRequest = async (query: string) => {
  return await fetch(import.meta.env.VITE_HOST_GRAPHQL, {
    method: "POST",

    headers: {
      "Content-Type": "application/json",
      Authorization: import.meta.env.VITE_ACCESS_TOKEN,
    },

    body: JSON.stringify({
      query,
    }),
  }).then((response) => response.json());
};
