import React from "react";
import Button from "../button/button";
import "./pagination.css";
import { PaginationDirection } from "../../enums/pagination-direction";

interface PaginationProps {
  hasPrevPage: boolean;
  hasNextPage: boolean;
  onChangePage: (direction: PaginationDirection) => void;
}

const Pagination = ({
  hasPrevPage,
  hasNextPage,
  onChangePage,
}: PaginationProps) => {
  return (
    <div className="pagination">
      <Button
        withArrow
        disabled={!hasPrevPage}
        onClick={() => onChangePage(PaginationDirection.PREV)}
      >
        Back
      </Button>
      <Button
        onClick={() => onChangePage(PaginationDirection.NEXT)}
        withArrow
        disabled={!hasNextPage}
        reverse
      >
        Next
      </Button>
    </div>
  );
};

export default Pagination;
