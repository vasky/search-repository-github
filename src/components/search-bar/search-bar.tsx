import React, { ChangeEvent } from "react";
import "./search-bar.css";
import IconCloseOutline from "../icons/icon-close-outline";
import IconSearch from "../icons/icon-search";

interface SearchBarProps {
  onChangeSearchValue: (value: string) => void;
  value: string;
}

const SearchBar = ({ value, onChangeSearchValue }: SearchBarProps) => {
  const handleSearch = (e: ChangeEvent<HTMLInputElement>) => {
    const nextValue = e.target.value;

    onChangeSearchValue(nextValue);
  };

  const handleReset = () => {
    onChangeSearchValue("");
  };

  return (
    <div className="search-bar">
      <form
        className="search-bar__form"
        onKeyPress={(e) => {
          if (e.code === "Enter") {
            e.preventDefault();
          }
        }}
      >
        <IconSearch width={22} />
        <input
          type="text"
          placeholder="Search repository"
          value={value}
          onChange={handleSearch}
        />
        {value?.length ? (
          <button className="search-bar__close-btn" onClick={handleReset}>
            <IconCloseOutline width={22} />
          </button>
        ) : (
          ""
        )}
      </form>
    </div>
  );
};

export default SearchBar;
