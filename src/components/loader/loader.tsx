import React from "react";
import "./loader.css";

const Loader = () => {
  return (
    <div className="loader">
      <svg
        width="80"
        height="80"
        viewBox="-20 -20 42 42"
        xmlns="http://www.w3.org/2000/svg"
        stroke="#0DBFFE"
        data-testid="oval-svg"
      >
        <g fill="none" fillRule="evenodd">
          <g
            transform="translate(1 1)"
            strokeWidth="2"
            data-testid="oval-secondary-group"
          >
            <circle
              strokeOpacity=".5"
              cx="0"
              cy="0"
              r="20"
              stroke="#D9F5FF"
              strokeWidth="2"
            />
            <path d="M20 0c0-9.94-8.06-20-20-20">
              <animateTransform
                attributeName="transform"
                type="rotate"
                from="0 0 0"
                to="360 0 0"
                dur="1s"
                repeatCount="indefinite"
              />
            </path>
          </g>
        </g>
      </svg>
    </div>
  );
};

export default Loader;
