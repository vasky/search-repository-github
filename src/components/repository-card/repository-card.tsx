import React from "react";
import "./repository-card.css";
import { NavLink } from "react-router-dom";
import { ROUTES } from "../../enums/routes";
import { generatePath } from "react-router";
import IconStar from "../icons/icon-star";
import { Repository } from "../../models/repository";
import formatDistanceToNow from "date-fns/formatDistanceToNow";
import { StringParam, useQueryParams } from "use-query-params";

const RepositoryCard = ({
  id,
  stargazerCount,
  shortDescriptionHTML,
  name,
  updatedAt,
  owner,
}: Repository) => {
  const [query] = useQueryParams({
    search: StringParam,
    after: StringParam,
    before: StringParam,
  });

  const { search, after, before } = query;

  return (
    <div className="repository-card">
      <div className="repository-card__top">
        <img className="repository-card__avatar" src={owner.avatarUrl} />
        <NavLink
          state={{ search, after, before }}
          to={generatePath(ROUTES.REPOSITORY, { repository: id })}
        >
          <div className="repository-card__name">{name}</div>
        </NavLink>
      </div>
      <div className="repository-card__description">{shortDescriptionHTML}</div>
      <div className="repository-card__bottom">
        <div className="repository-card__stars">
          <IconStar />
          <span className="repository-card__stars-number">
            {stargazerCount}
          </span>
        </div>
        <div className="repository-card__separate">·</div>
        <div className="repository-card__updated">
          Updated {formatDistanceToNow(new Date(updatedAt))}
        </div>
      </div>
    </div>
  );
};

export default RepositoryCard;
