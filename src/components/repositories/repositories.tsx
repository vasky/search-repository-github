import React from "react";
import "./repositories.css";
import { Repository } from "../../models/repository";
import RepositoryCard from "../repository-card/repository-card";

interface RepositoriesProps {
  items: Repository[];
}

const Repositories = ({ items }: RepositoriesProps) => {
  return (
    <div className="repositories">
      {items.map((item) => (
        <RepositoryCard key={item.id} {...item} />
      ))}
    </div>
  );
};

export default Repositories;
