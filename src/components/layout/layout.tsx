import React from 'react';
import './layout.css'

interface LayoutProps {
  children: JSX.Element
}

const Layout = ({children}: LayoutProps) => {
  return (
    <div className='container layout'>
      {children}
    </div>
  );
};

export default Layout;