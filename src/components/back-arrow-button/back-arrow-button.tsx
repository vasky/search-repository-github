import React from "react";
import "./back-arrow-button.css";
import IconArrow from "../icons/icon-arrow";
import { NavLink, useLocation } from "react-router-dom";
import { ROUTES } from "../../enums/routes";

interface BackArrowButtonProps {
  children: JSX.Element;
  to: ROUTES;
}

const BackArrowButton = ({ children, to }: BackArrowButtonProps) => {
  const location = useLocation();
  return (
    <NavLink state={location.state} to={to} className="back-arrow-button">
      <IconArrow width={22} stroke="#151400" />
      <span className="back-arrow-button__text">{children}</span>
    </NavLink>
  );
};

export default BackArrowButton;
