import React from "react";

interface IconArrowProps {
  stroke: string;
  className?: string;
  width?: number;
}

const IconArrow = ({
  stroke = "#151400",
  className,
  width = 16,
}: IconArrowProps) => {
  return (
    <svg
      width={`${width}px`}
      height={`${width}px`}
      viewBox="0 0 16 16"
      fill="none"
      className={className}
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M9.66699 4.33398L6.86715 7.6938C6.55811 8.06465 6.55811 8.60332 6.86715 8.97417L9.66699 12.334"
        stroke={stroke}
        strokeWidth="2"
        strokeLinecap="round"
      />
    </svg>
  );
};

export default IconArrow;
