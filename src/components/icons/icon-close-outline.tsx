import * as React from "react";
import { IconProps } from "../../models/icon";

function IconCloseOutline({ width }: IconProps) {
  return (
    <svg
      viewBox="0 0 512 512"
      fill="currentColor"
      height={`${width}px`}
      width={`${width}px`}
    >
      <path
        fill="none"
        stroke="currentColor"
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth={32}
        d="M368 368L144 144M368 144L144 368"
      />
    </svg>
  );
}

export default IconCloseOutline;
