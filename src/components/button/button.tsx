import "./button.css";
import IconArrow from "../icons/icon-arrow";

interface ButtonProps {
  children: JSX.Element;
  onClick: VoidFunction;
  withArrow?: boolean;
  reverse?: boolean;
  disabled?: boolean;
}

const Button = ({
  children,
  onClick,
  withArrow,
  disabled,
  reverse,
}: ButtonProps) => {
  const arrowFill = disabled ? "#CDCDCD" : "#0DBFFE";
  return (
    <button
      className={`button ${reverse ? "button--reverse" : ""}`}
      onClick={onClick}
      disabled={disabled}
    >
      {withArrow && <IconArrow stroke={arrowFill} className="button__arrow" />}
      {children}
    </button>
  );
};

export default Button;
