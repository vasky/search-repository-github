export interface Language {
  color: string;
  id: string;
  name: string;
  size: string;
}
