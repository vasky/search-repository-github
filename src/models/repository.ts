import { Language } from "./language";

export interface Repository {
  id: string;
  description: string;
  shortDescriptionHTML: string;
  name: string;
  owner: {
    avatarUrl: string;
    url: string;
    login: string;
  };
  url: string;
  descriptionHTML: JSX.Element;
  stargazerCount: number;
  updatedAt: string;
  languages: { nodes: Language[] };
}
