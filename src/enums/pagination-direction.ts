export enum PaginationDirection {
  PREV = "PREV",
  NEXT = "NEXT",
}
